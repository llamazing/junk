## Project Background

(Provide background about your quest project: characters, setting, game mechanics, etc. as it is relevant to the quest log quest you are trying to write)

## Quest Description

(Give a detailed description of what you want the player to do at each step of the quest, and list any quest items, NPCs and locations that are related to the quest)

### Mock-ups

(describe how you want the quest log description text to appear during various phases, and include mock-up screenshots if applicable)

### Sample Code

(If you have started to make any attempts at writing the dialogs.dat description text or objectives.dat entrie(s), then paste the code you have so far)

```
Example Code
```

/label ~help